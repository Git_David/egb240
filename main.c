/**
 * main.c - EGB240 Digital Voice Recorder Skeleton Code
 *
 * This code provides a skeleton implementation of a digital voice 
 * recorder using the Teensy microcontroller and QUT TensyBOBv2 
 * development boards. This skeleton code demonstrates usage of
 * the EGB240DVR library, which provides functions for recording
 * audio samples from the ADC, storing samples temporarily in a 
 * circular buffer, and reading/writing samples to/from flash
 * memory on an SD card (using the FAT file system and WAVE file
 * format. 
 *
 * This skeleton code provides a recording implementation which 
 * samples CH0 of the ADC at 8-bit, 15.625kHz. Samples are stored 
 * in flash memory on an SD card in the WAVE file format. The 
 * filename is set to "EGB240.WAV". The SD card must be formatted 
 * with the FAT file system. Recorded WAVE files are playable on 
 * a computer.
 * 
 * LED4 on the TeensyBOBv2 is configured to flash as an 
 * indicator that the programme is running; a 1 Hz, 50 % duty
 * cycle flash should be observed under normal operation.
 *
 * A serial USB interface is provided as a secondary control and
 * debugging interface. Errors will be printed to this interface.
 *
 * Version: v1.0
 *    Date: 10/04/2016
 *  Author: Mark Broadmeadow
 *  E-mail: mark.broadmeadow@qut.edu.au
 */  

 /************************************************************************/
 /* INCLUDED LIBRARIES/HEADER FILES                                      */
 /************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>

//#include <util/delay.h>

#include <stdio.h>

#include "serial.h"
#include "timer.h"
#include "wave.h"
#include "buffer.h"
#include "adc.h"
#include "E:\QUT\EGB240 Electronic Design\Assignement 2\EGB240DVR_Library&SkeletonCode_v1.0(1)\lib\usb_serial\usb_serial.h"
/************************************************************************/
/* ENUM DEFINITIONS                                                     */
/************************************************************************/
enum {
	DVR_STOPPED,
	DVR_RECORDING,
	DVR_PLAYING
};

/************************************************************************/
/* GLOBAL VARIABLES                                                     */
/************************************************************************/
uint16_t pageCount = 0;	// Page counter - used to terminate recording
uint16_t newPage = 0;	// Flag that indicates a new page is available for read/write
uint8_t stop = 0;		// Flag that indicates playback/recording is complete
uint32_t samples_main =0;    // Number of samples
//int samples_count =0;

#define TOP 255
/************************************************************************/
/* FUNCTION PROTOTYPES                                                  */
/************************************************************************/
void pageFull();
void pageEmpty();
void inOut_init();
void init_timer1();

/************************************************************************/
/* INITIALISATION FUNCTIONS                                             */
/************************************************************************/

// Initialise PLL (required by USB serial interface, PWM)
void pll_init() {
	PLLFRQ = 0x6A; // PLL = 96 MHz, USB = 48 MHz, TIM4 = 64 MHz
}

// Configure system clock for 16 MHz
void clock_init() {
	CLKPR = 0x80;	// Prescaler change enable
	CLKPR = 0x00;	// Prescaler /1, 16 MHz
}

void inOut_init(){
	
		DDRB |= 0b01000000;		// sets JOUT as an output. Once it is set as an output
		DDRF &= 0b00001110;		// Set PORTF 7-4 as inputs (PBs) Set PORTF 0 as input (Jin)
		DDRD |= 0b01110000;		// Set PORTD 6-4 as outputs (LEDs)
		PORTD &= 0b00001111;	// turn LEDs off
		
		
	
}


//Timer1 is a 16 bit Timer
void init_timer1(){
		TCCR1A = 0b00100001; // PWM TOP = 255, Phase correct, 8bit
		TCCR1B = 0b00000101; // Sets clk/1024 = 15.625 kHz at 16MHz clock
}

// Initialise DVR subsystems and enable interrupts
void init() {
	cli();			// Disable interrupts
	clock_init();	// Configure clocks
	pll_init();     // Configure PLL (used by Timer4 and USB serial)
	serial_init();	// Initialise USB serial interface (debug)
	timer_init();	// Initialise timer (used by FatFs library)
	buffer_init(pageFull, pageEmpty);  // Initialise circular buffer (must specify callback functions)
	adc_init();		// Initialise ADC
	
	inOut_init(); // Initialises the Buttons and LEDs
		
	sei();			// Enable interrupts
	
	// Must be called after interrupts are enabled
	wave_init();	// Initialise WAVE file interface
}

/************************************************************************/
/* CALLBACK FUNCTIONS FOR CIRCULAR BUFFER                               */
/************************************************************************/

// CALLED FROM BUFFER MODULE WHEN A PAGE IS FILLED WITH RECORDED SAMPLES
void pageFull() { //Called on Record
	if(!(--pageCount)) {
		// If all pages have been read
		adc_stop();		// Stop recording (disable new ADC conversions)
		stop = 1;		// Flag recording complete
	} else {
		newPage = 1;	// Flag new page is ready to write to SD card
	}
}

// CALLED FROM BUFFER MODULE WHEN A NEW PAGE HAS BEEN EMPTIED
void pageEmpty() { //Called on Playback
	// TODO: Implement code to handle "page empty" callback 
	if(!(--pageCount)){
		//toggle page
		//buffer_readPage();
		stop = 1;
	}else{
		newPage = 1;
	}
}

/************************************************************************/
/* RECORD/PLAYBACK ROUTINES                                             */
/************************************************************************/

// Initiates a record cycle
void dvr_record() {
	buffer_reset();		// Reset buffer state
	
	pageCount = 305;	// Maximum record time of 10 sec
	newPage = 0;		// Clear new page flag
	
	wave_create();		// Create new wave file on the SD card
	adc_start();		// Begin sampling

	 //TODO: Turn Red LED on while recording
	usb_serial_write("Switching on RED LED!\n\r",25);
	PORTD |= (1 << 5);			//Switch on red LED
	PORTD &= ~((1<<4)|(1<<6));  //Turn off green and blue
}



// TODO: Implement code to initiate playback and to stop recording/playback.

/************************************************************************/
/* MAIN LOOP (CODE ENTRY)                                               */
/************************************************************************/
int main(void) {
	uint8_t state = DVR_STOPPED;	// Start DVR in stopped state
	int state_changed = 1;
	
	// Initialization
	init();	
	
	//while(!serial_available());  //enables the connection to the pc and waits for a character to be sent
	printf("printf working.");
	usb_serial_write("Connection has been initialized!\n\r",40);
	//printf("Connection has been initialized!\n\r");

	// Loop forever (state machine)
    for(;;) {

		// Switch depending on state
		switch (state) {
			case DVR_STOPPED:
				// Turn Blue LED on and write message to console
				if (state_changed){
				usb_serial_write("DVR_STOPPED\n\r",30);
				usb_serial_write("Switching on BLUE LED\n\r",33);
				state_changed = 0;
				}
				//Blue LED on
				PORTD |= (1 << 6);			//Switch on blue LED
				PORTD &= ~((1<<4)|(1<<5));  //Turn off green and red
				
				// Switch from stop state to recording or playing state
				 if (~(PINF)>>5 & 1) {	//Button S2 starts recording
					usb_serial_write("Button S2 has been pressed\n\r",32);
					usb_serial_write("Recording...\n\n\r",15);	// Output status to console
					dvr_record();			// Initiate recording // handles LEDs
					state = DVR_RECORDING;	// Transition to "recording" state
					state_changed = 1;
				 }else if (~(PINF)>>4 & 1){//Button S1 starts playback
					 state= DVR_PLAYING;	//Transition to "playing" state
					 PORTD |= (1 << 4);			//Switch on green LED
					 PORTD &= ~((1<<5)|(1<<6));  //Turn off red and blue
					 usb_serial_write("Button S1 has been pressed\n\r",32);
					 usb_serial_write("State is DVR_PLAYING\n\n\r",30);
					 state_changed = 1;
				 }
				break;
			case DVR_RECORDING:
				 //STOP function if Button S3 is pressed
				 if (( ~PINF >> 6 )&1) {
					usb_serial_write("Button S3 has been pressed!\n\r",30);
					
					pageCount = 1;	// Finish recording last 
					state = DVR_STOPPED; // Transition to "stopped" state
					usb_serial_write("State is DVR_STOPPED (from record by button)\n\n\r",45);
					break;
				 }
			
				// Write samples to SD card when buffer page is full
				if (newPage) {
					newPage = 0;	// Acknowledge new page flag
					wave_write(buffer_readPage(), 512);
					usb_serial_write("Writing to file!\n\r",20 );
				} else if (stop) {
					// Stop is flagged when the last page has been recorded
					stop = 0;							// Acknowledge stop flag
					wave_write(buffer_readPage(), 512);	// Write final page
					wave_close();						// Finalise WAVE file 
					usb_serial_write("Recording DONE!\n\r",30);	// Print status to console
					state = DVR_STOPPED;				// Transition to stopped state
					usb_serial_write("State is DVR_STOPPED from record \n\r",40);//Message on Console
				}
				break;
			case DVR_PLAYING:
				// Playback function
				if (state_changed){//only done once at first call of DVR_PLAYING
					//Debug message
					usb_serial_write("Playing some sound!\n\r",30);
					state_changed=0;
					samples_main = wave_open(); //open file
				}
				//PLAYBACK CODE
				if (!stop){				     
					wave_read(buffer_writePage(),512);//read wave file and write it to the buffer 512 samples = 1 page
					uint8_t	output = buffer_dequeue(); // set output to read value from buffer and progresses the read pointer one line
					OCR1B= output;  //output compare register 1 is set to 
		 			pageEmpty();   // check if page is empty or if all pages have been read
					
				}else { // stop = 1
					wave_close(); //Close wave file
					state_changed =1 ;
					state = DVR_STOPPED;  // return to state "stopped"
					usb_serial_write("State is DVR_STOPPED now(from playing)!\n\n\r",30);
					OCR1B = 0;  // reset output register
				}
				
				
				//state = DVR_STOPPED;	
				if((~PINF >> 6)&1){			//Button S3 is pressed
					printf("Button S3 has been pressed!\n\r");
					state = DVR_STOPPED;	//return to state "stopped"
					printf("State is DVR_STOPPED now(from playing)!\n\n\r");
					state_changed = 1;
					wave_close();  // Close Wave file
					OCR1B = 0;   // reset output register 
				}
				break;
			default:
				// Invalid state, return to valid idle state (stopped)
				printf("ERROR: State machine in main entered invalid state!\n");
				state = DVR_STOPPED;
				break;

		} // END switch(state)
			
	} // END for(;;)

}
